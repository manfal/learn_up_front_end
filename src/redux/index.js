/**
 * Created by anfal on 12/3/2016.
 */

import config from '../config';
import debug_enabled_store from './debug_enabled_store';
import debug_disabled_store from './debug_disabled_store';

let store = (config.debug) ? debug_enabled_store : debug_disabled_store;

export default store;