/**
 * Created by anfal on 12/12/2016.
 */

import ActionNames from '../../action_names';

export function parse_fetched_users_list(data) {
    return {
        type: ActionNames.PARSE_FETCHED_USERS_LIST,
        data: data
    }
}

export function parse_fetched_university_data(data) {
    return {
        type: ActionNames.PARSE_FETCHED_UNIVERSITY_DATA,
        data: data
    }
}

export function insert_user_failed(data) {
    return {
        type: ActionNames.INSERT_USER_FAILED,
        data: data
    }
}

export function delete_user_failed(data) {
    return {
        type: ActionNames.DELETE_USER_FAILED,
        data: data
    }
}

export function parse_university_department_list(data) {
    return {
        type: ActionNames.PARSE_UNIVERSITY_DEPARTMENT_LIST,
        data: data
    }
}

export function clear_status_and_message() {
    return {
        type: ActionNames.CLEAR_STATUS_AND_MESSAGE
    }
}

export function delete_user_succeeded(data) {
    return {
        type: ActionNames.DELETE_USER_SUCCEEDED,
        data: data
    }
}

export function insert_user_succeeded(data) {
    return {
        type: ActionNames.INSERT_USER_SUCCEEDED,
        data: data
    }
}

export function update_pagination_index(index) {
    return {
        type: ActionNames.UPDATE_PAGINATION_INDEX,
        data: index
    }
}

export function reset_users_page_state() {
    return {
        type: ActionNames.RESET_USERS_PAGE_STATE
    }
}

export function parse_edit_user_data(data) {
    return {
        type: ActionNames.PARSE_EDIT_USER_DATA,
        data: data
    }
}

export function reset_edit_user_data() {
    return {
        type: ActionNames.RESET_EDIT_USER_DATA
    }
}