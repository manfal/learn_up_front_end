/**
 * Created by anfal on 12/1/2016.
 */

import ActionNames from './action_names';

export function parse_sign_in_results(api_response) {
    return {
        type: ActionNames.PARSE_FETCHED_DATA,
        data: api_response
    }
}

export function assign_data_from_storage_to_state(data_object) {
    return {
        type: ActionNames.ASSIGN_DATA_FROM_STORAGE,
        data: data_object
    }
}

export function logout_user() {
    return {
        type: ActionNames.LOGOUT_USER
    }
}