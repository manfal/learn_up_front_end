/**
 * Created by anfal on 12/5/2016.
 */

import ActionNames from './action_names';

export function data_fetch_failed() {
    return {
        type: ActionNames.FETCH_FAILED
    }
}

export function show_loader() {
    return {
        type: ActionNames.SHOW_LOADER
    }
}

export function hide_loader() {
    return {
        type: ActionNames.HIDE_LOADER
    }
}

export function show_dimmer() {
    return {
        type: ActionNames.SHOW_DIMMER
    }
}

export function hide_dimmer() {
    return {
        type: ActionNames.HIDE_DIMMER
    }
}