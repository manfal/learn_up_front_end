/**
 * Created by anfal on 12/3/2016.
 */
import { createStore, applyMiddleware } from 'redux';
import ThunkMiddleWare from 'redux-thunk';
import CreateLogger from 'redux-logger';
import Reducers from './reducers';

const loggerMiddleware = CreateLogger();
const store = createStore(Reducers, applyMiddleware(
    ThunkMiddleWare,
    loggerMiddleware
));

export default store;