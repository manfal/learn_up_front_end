/**
 * Created by anfal on 12/12/2016.
 */

import { show_loader, hide_loader } from '../../../actions/GenericActions';
import {
    parse_fetched_users_list,
    parse_fetched_university_data,
    parse_university_department_list,
    insert_user_failed,
    insert_user_succeeded,
    delete_user_failed,
    delete_user_succeeded,
    update_pagination_index,
    parse_edit_user_data
} from '../../../actions/dashboard/admin/UsersActions';
import UsersController from '../../../../controllers/dashboard/admin/UsersController';
import utilities from '../../../../controllers/utilities';
import config from '../../../../config';

export function get_all_users(current_page_index, filter_user_text="", role="") {
    return dispatch => {
        dispatch(show_loader());
        UsersController.get_all_users(
            current_page_index,
            config.pagination.max_items_per_page,
            filter_user_text,
            role
        )
            .then(response => {
                dispatch(parse_fetched_users_list(response));
            })
            .then(() => {
                dispatch(hide_loader());
            })
            .catch(error => {
                dispatch(hide_loader());
        });
    }
}

export function get_university_data() {
    return dispatch => {
        UsersController.get_university_data()
            .then(response => {
                dispatch(parse_fetched_university_data(response));
            })
            .catch(error => {

            });
        UsersController.get_university_department_list()
            .then(response => {
                dispatch(parse_university_department_list(response))
            })
            .catch(error => {

        });
    }
}

export function insert_new_user(user_data_object, current_page_index) {
    return dispatch => {
        UsersController.insert_new_user(user_data_object)
            .then(response => {
                let status = response.data[0].status;
                if(utilities.keys_object.SUCCESS === status) {
                    dispatch(insert_user_succeeded(response));
                    dispatch(get_all_users(current_page_index));
                } else {
                    dispatch(insert_user_failed(response));
                }
            })
    }
}

export function delete_users(user_ids, current_page_index) {
    return dispatch => {
        UsersController.delete_users(user_ids)
            .then(response => {
                if(utilities.keys_object.SUCCESS === response.status) {
                    dispatch(delete_user_succeeded(response));
                    dispatch(get_all_users(current_page_index));
                } else {
                    dispatch(delete_user_failed(response));
                }
            })
    }
}

export function update_user_page_index(index) {
    return dispatch => {
        dispatch(get_all_users(index));
        dispatch(update_pagination_index(index));
    }
}

export function filter_users(filter_user_text, current_page_index) {
    return dispatch => {
        dispatch(get_all_users(current_page_index, filter_user_text));
    }
}

export function filter_users_by_role(current_page_index, role) {
    return dispatch => {
        dispatch(get_all_users(current_page_index, "",role))
    }
}

export function fetch_edit_user_data(user_id) {
    return dispatch => {
        dispatch(show_loader());
        UsersController.get_single_user_data(user_id)
            .then(response => {
                dispatch(parse_edit_user_data(response))
            })
            .then(() => {
                dispatch(hide_loader());
            });
    }
}