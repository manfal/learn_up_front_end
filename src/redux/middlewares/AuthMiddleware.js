/**
 * Created by anfal on 12/1/2016.
 */

import AuthController from '../../controllers/AuthController';
import { parse_sign_in_results, logout_user } from '../actions/AuthActions';
import { show_loader, hide_loader } from '../actions/GenericActions';
import { data_fetch_failed } from '../actions/GenericActions';
import utilities from '../../controllers/utilities';

export function initiate_sign_in(username, password, remember_me) {
    return dispatch => {
        dispatch(show_loader());
        AuthController.authenticate_user(username, password, remember_me)
            .then(response => {
                dispatch(parse_sign_in_results(response));
            }).then(() => {
            dispatch(hide_loader());
            utilities.in_app_routings.redirectUserToDashboard();
        }).catch(error => {
            dispatch(hide_loader());
            dispatch(data_fetch_failed());
        });
    }
}

export function logout() {
    return dispatch => {
        AuthController.clear_user_data_from_storage();
        dispatch(logout_user());
        utilities.in_app_routings.redirectUserToLoginPage();
    };
}