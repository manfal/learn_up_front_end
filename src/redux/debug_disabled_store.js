/**
 * Created by anfal on 12/3/2016.
 */
import { createStore, applyMiddleware } from 'redux';
import ThunkMiddleWare from 'redux-thunk';
import Reducers from './reducers';

const store = createStore(Reducers, applyMiddleware(ThunkMiddleWare));

export default store;