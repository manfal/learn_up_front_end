/**
 * Created by anfal on 11/29/2016.
 */

import ActionNames from '../actions/action_names';
import utilities from '../../controllers/utilities';
import SignInController from '../../controllers/AuthController';

const default_auth_state = {
    is_logged_in: false,
    message: '',
    active_role: '',
    active_permission_set: [],
    data: {}
};

const auth_reducer = (state=default_auth_state, action) => {
    switch(action.type) {
        case ActionNames.PARSE_FETCHED_DATA:
            let returned_json = action.data;
            let new_state = {};
            if(utilities.keys_object.FAIL === returned_json.status) {
                new_state = {
                    ...state,
                    message: returned_json.message
                };
            } else if(utilities.keys_object.SUCCESS === returned_json.status) {
                let role = Object.keys(returned_json.data.permission_set)[0].toLowerCase();
                new_state = {
                    ...state,
                    is_logged_in: true,
                    data: returned_json.data,
                    active_role: role,
                    active_permission_set: returned_json.data.permission_set[role]
                };
                SignInController.store_user_data_in_storage(new_state);
            }
            return new_state;
        case ActionNames.ASSIGN_DATA_FROM_STORAGE:
            return Object.assign({}, action.data);
        case ActionNames.LOGOUT_USER:
            return {
                ...default_auth_state
            };
        case ActionNames.FETCH_FAILED:
            return {
                ...state,
                message: utilities.language.login_failed_text
            };
        default:
            return state
    }
};

export default auth_reducer;