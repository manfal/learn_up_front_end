/**
 * Created by anfal on 12/12/2016.
 */


import ActionNames from '../../../actions/action_names';
import UsersController from '../../../../controllers/dashboard/admin/UsersController';
import utilities from '../../../../controllers/utilities';

const default_users_state = {
    users_list: [],
    roles_list: [],
    batch_list: [],
    department_list: [],
    message: '',
    status: '',
    action_owner: '',
    edit_user_data: {},
    current_user_table_pagination_index: 1,
    total_user_table_pages: 0
};

const users_reducer = (state=default_users_state, action) => {
    switch(action.type) {
        case ActionNames.PARSE_FETCHED_USERS_LIST:
            let returned_json = action.data;
            let new_state = {};
            if(utilities.keys_object.FAIL === returned_json.status) {
                new_state = {
                    ...state
                }
            } else if(utilities.keys_object.SUCCESS === returned_json.status) {
                let response = action.data.data;
                let total_number_of_users = response.number_of_users;
                let users_list = response.users;
                new_state = {
                    ...state,
                    users_list: UsersController.parse_users_information_to_array(users_list),
                    total_user_table_pages: UsersController.get_max_number_of_pages_for_pagination(total_number_of_users)
                }
            }
            return new_state;
        case ActionNames.PARSE_FETCHED_UNIVERSITY_DATA:
            return {
                ...state,
                roles_list: action.data.data.roles,
                batch_list: action.data.data.batches
            };
        case ActionNames.DELETE_USER_FAILED:
            return {
                ...state,
                status: action.data.status,
                message: action.data.message,
                action_owner: utilities.keys_object.DELETE_USER
            };
        case ActionNames.INSERT_USER_FAILED:
            return {
                ...state,
                status: action.data.data[0].status,
                message: action.data.data[0].message,
                action_owner: utilities.keys_object.CREATE_OR_UPDATE_USER
            };
        case ActionNames.PARSE_UNIVERSITY_DEPARTMENT_LIST:
            return {
                ...state,
                department_list: action.data.data
            };
        case ActionNames.CLEAR_STATUS_AND_MESSAGE:
            return {
                ...state,
                status: '',
                message: '',
                action_owner: ''
            };
        case ActionNames.INSERT_USER_SUCCEEDED:
            return {
                ...state,
                status: action.data.status,
                action_owner: utilities.keys_object.ADD_USER
            };
        case ActionNames.DELETE_USER_SUCCEEDED:
            return {
                ...state,
                status: action.data.status,
                action_owner: utilities.keys_object.DELETE_USER
            };
        case ActionNames.UPDATE_PAGINATION_INDEX:
            return {
                ...state,
                current_user_table_pagination_index: action.data
            };
        case ActionNames.RESET_USERS_PAGE_STATE:
            return {
                ...default_users_state
            };
        case ActionNames.PARSE_EDIT_USER_DATA:
            return {
                ...state,
                edit_user_data: action.data.data.users[0]
            };
        case ActionNames.RESET_EDIT_USER_DATA:
            return {
                ...state,
                edit_user_data: {}
            };
        default:
            return state;
    }
};

export default users_reducer;