/**
 * Created by anfal on 12/2/2016.
 */


const default_dashboard_state = {};

const dashboard_reducer = (state=default_dashboard_state, action) => {
    switch(action.type) {
        default:
            return state;
    }
};

export default dashboard_reducer;