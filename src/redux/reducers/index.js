/**
 * Created by anfal on 11/28/2016.
 */

import { combineReducers } from 'redux';
import auth_reducer from './auth_reducer';
import dashboard_reducer from './dashboard/dashboard_reducer';
import generic_reducer from './generic_reducer';
import users_reducer from './dashboard/admin/users_reducer';

const root_reducer = combineReducers({
    auth_reducer,
    dashboard_reducer,
    generic_reducer,
    users_reducer
});

export default root_reducer;