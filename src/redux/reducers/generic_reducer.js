/**
 * Created by anfal on 12/10/2016.
 */

import ActionNames from '../actions/action_names';

const default_generic_state = {
    is_loader_active: false,
    is_dimmer_active: false
};

const generic_reducer = (state=default_generic_state, action) => {
    switch(action.type) {
        case ActionNames.SHOW_LOADER:
            return {
                ...state,
                is_loader_active: true
            };
        case ActionNames.HIDE_LOADER:
            return {
                ...state,
                is_loader_active: false
            };
        case ActionNames.SHOW_DIMMER:
            return {
                ...state,
                is_dimmer_active: true
            };
        case ActionNames.HIDE_DIMMER:
            return {
                ...state,
                is_dimmer_active: false
            };
        default:
            return state;
    }
};

export default generic_reducer;