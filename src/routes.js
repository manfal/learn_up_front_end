/**
 * Created by anfal on 11/28/2016.
 */

import React from 'react';
import { Route } from 'react-router';
import utilities from './controllers/utilities';

/**Components to be displayed using router**/
import StateFullSignInPage from './components/statefull_containers/statefull_sign_in_page';
import StateFullDashboard from './components/statefull_containers/dashboard/statefull_dashboard';
import AuthenticationStatusFilter from './components/security/authentication_components/authentication_status_filter';
import StateFullUsersPage from './components/statefull_containers/dashboard/admin/statefull_users_page';
import StateFullCourseSettingsPage from './components/statefull_containers/dashboard/admin/statefull_course_settings';
import NotFoundPage from './components/parent_containers/not_found_page';
import StateFullDepartments from './components/statefull_containers/dashboard/admin/statefull_departments';

export default <Route path="/" component={AuthenticationStatusFilter(StateFullSignInPage)}>
    <Route path="dashboard" component={AuthenticationStatusFilter(StateFullDashboard)}>
        <Route
            path="users"
            authorize={[utilities.system_users.admin]}
            component={AuthenticationStatusFilter(StateFullUsersPage)}
        />
        <Route
            path="course_settings"
            authorize={[utilities.system_users.admin]}
            component={AuthenticationStatusFilter(StateFullCourseSettingsPage)}
        />
        <Route
            path="departments"
            authorize={[utilities.system_users.admin]}
            component={AuthenticationStatusFilter(StateFullDepartments)}
        />
    </Route>
    <Route path="*" component={NotFoundPage}/>
</Route>