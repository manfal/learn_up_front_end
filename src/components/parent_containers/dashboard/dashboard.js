/**
 * Created by anfal on 11/28/2016.
 */

import React, { Component, PropTypes } from 'react'
import DashboardTopMenu from '../../compound_components/menu/dashboard_top_menu';
import SideBarMenu from '../../individual_components/menu/side_bar_menu';
import FullScreenPageLoader from '../../individual_components/loaders/full_screen_page_loader';
import utilities from '../../../controllers/utilities';
import '../../../assets/custom_css/dashboard/dashboard.css';
import FullPageLightGreyBackgroundDiv from '../../individual_components/div/full_page_light_grey_background_div';

class Dashboard extends Component {

    static propTypes = {
        auth_state: PropTypes.object.isRequired,
        logout_user: PropTypes.func.isRequired,
        children: PropTypes.node
    };

    constructor(props) {
        super(props);
        const { auth_state } = this.props;
        let user_info = auth_state.data.user_info;
        if (undefined !== user_info) {
            this.dashboard_info_object = {
                first_name: user_info.first_name,
                last_name: user_info.last_name
            };
        }
        this.state = {
            current_path: this.props.location.pathname
        };
        this.active_role_menu_items = utilities.menu_items[auth_state.active_role];

        this.set_currently_active_menu_item_url = this.set_currently_active_menu_item_url.bind(this);
    }

    set_currently_active_menu_item_url(url) {
        this.setState({current_path: url});
    }

    render() {

        const { logout_user, generic_state } = this.props;

        return <FullScreenPageLoader
            loading_text={utilities.language.loading_text}
            is_loader_active={generic_state.is_loader_active}
            is_dimmer_active={generic_state.is_dimmer_active}
        >
            <FullPageLightGreyBackgroundDiv>
                <DashboardTopMenu
                    {...this.dashboard_info_object}
                    logout_user={logout_user}
                />
                <div className="ui grid dashboard_page">
                    <div className="row ">
                        <div className="three wide column">
                            <SideBarMenu
                                menu_items={this.active_role_menu_items}
                                current_path={this.state.current_path}
                                set_currently_active_menu_item={this.set_currently_active_menu_item_url}
                            />
                        </div>
                        <div className="thirteen wide column">
                            {this.getChildrenToRender()}
                        </div>
                    </div>
                </div>
            </FullPageLightGreyBackgroundDiv>
        </FullScreenPageLoader>
    }

    getChildrenToRender() {

        const { children } = this.props;

        if(children) {
            return children;
        } else {
            return <h1>dashboard</h1>
        }
    }
}

export default Dashboard;