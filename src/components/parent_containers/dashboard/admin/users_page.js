/**
 * Created by anfal on 12/6/2016.
 */
import React from 'react';
import PlainCardWithImage from '../../../individual_components/cards/plain_card_with_image';
import '../../../../assets/custom_css/dashboard/admin/users_page.css';
import AddUserNewImage from '../../../../assets/images/dashboard/admin/users_page/add_user_new.png';
import AddUserBulkImage from '../../../../assets/images/dashboard/admin/users_page/add_user_bulk.png';
import UnstackableVeryBasicStripedUsersTable from '../../../compound_components/tables/unstackable_very_basic_striped_users_table';
import CreateUpdateUserModal from '../../../compound_components/modal/create_update_user_modal';
import DeleteUserModal from '../../../compound_components/modal/delete_a_user_modal';
import utilities from '../../../../controllers/utilities';
import SimplePaginationMenu from '../../../individual_components/pagination/simple_pagination_menu';
import PlainCircularBorderInputWithRightButton from '../../../individual_components/inputs/plain_circular_border_input_with_right_button';
import PlainDropDownWithCircularBorder from '../../../individual_components/dropdowns/plain_dropdown_with_circular_border';
import { Scrollbars } from 'react-custom-scrollbars';
import AuthorizationAwareComponent from '../../../security/authorization_components/authorization_aware_component';

class UsersPage extends AuthorizationAwareComponent {

    constructor(props) {
        super(props);
        this.close_popup = this.close_popup.bind(this);
        this.add_new_user = this.add_new_user.bind(this);
        this.open_delete_user_popup = this.open_delete_user_popup.bind(this);
        this.open_popup = this.open_popup.bind(this);
        this.delete_users = this.delete_users.bind(this);
        this.filter_users = this.filter_users.bind(this);
        this.alt_filter_user_action = this.alt_filter_user_action.bind(this);
        this.filter_users_on_role = this.filter_users_on_role.bind(this);
        this.handle_edit_user = this.handle_edit_user.bind(this);
        this.state = {
            is_add_new_user_popup_active: false,
            is_edit_user_popup_active: false,
            is_delete_user_popup_active: false
        };
        this.delete_user_ids = null;
    }

    componentWillMount() {
        super.componentWillMount();
        this.props.get_university_data();
        this.props.get_all_users(this.props.users_state.current_user_table_pagination_index);
    }

    open_popup(popup_type) {
        this.props.show_dimmer();
        switch (popup_type) {
            case utilities.keys_object.ADD_USER:
                this.setState({
                    is_add_new_user_popup_active: true
                });
                break;
            case utilities.keys_object.DELETE_USER:
                this.setState({
                    is_delete_user_popup_active: true
                });
                break;
            case utilities.keys_object.EDIT_USER:
                this.setState({
                   is_edit_user_popup_active: true
                });
                break;
            default:
                break;
        }
    }

    close_popup(popup_type) {
        if('' !== this.props.users_state.status || '' !== this.props.users_state.message) {
            this.props.clear_status_and_message();
        }
        switch (popup_type) {
            case utilities.keys_object.ADD_USER:
                this.setState({
                    is_add_new_user_popup_active: false
                });
                break;
            case utilities.keys_object.DELETE_USER:
                this.setState({
                    is_delete_user_popup_active: false
                });
                break;
            case utilities.keys_object.EDIT_USER:
                this.setState({
                    is_edit_user_popup_active: false
                });
                break;
            default:
                break;
        }
        this.props.hide_dimmer();
    }

    add_new_user(user_data_object) {
        const { auth_state } = this.props;
        user_data_object = {
            ...user_data_object,
            university: auth_state.data.user_info.university_code
        };
        let users = [user_data_object];
        this.props.insert_new_user(users, this.props.users_state.current_user_table_pagination_index);
    }

    open_delete_user_popup(user_ids) {
        this.delete_user_ids = user_ids;
        this.open_popup(utilities.keys_object.DELETE_USER);
    }

    delete_users() {
        if(null !== this.delete_user_ids) {
            this.props.delete_users(this.delete_user_ids, this.props.users_state.current_user_table_pagination_index);
            this.delete_user_ids = null;
        }
    }

    filter_users(text) {
        this.props.filter_users(text, this.props.users_state.current_user_table_pagination_index);
    }

    alt_filter_user_action() {
        this.props.get_all_users(this.props.users_state.current_user_table_pagination_index);
    }

    filter_users_on_role(role) {
        this.props.filter_users_by_role(
            this.props.users_state.current_user_table_pagination_index,
            role
        );
    }

    handle_edit_user(user_id) {
        this.props.fetch_edit_user_data(user_id);
        this.open_popup(utilities.keys_object.EDIT_USER);
    }

    render() {

        const { users_state } = this.props;
        return <div className="ui grid">
            <CreateUpdateUserModal
                is_popup_active={this.state.is_add_new_user_popup_active}
                close_popup={() => {this.close_popup(utilities.keys_object.ADD_USER)}}
                roles_list={users_state.roles_list}
                batch_list={users_state.batch_list}
                department_list={users_state.department_list}
                add_new_user={this.add_new_user}
                insert_status={users_state.status}
                insert_message={users_state.message}
                action_owner={users_state.action_owner}
            />
            <DeleteUserModal
                is_popup_active={this.state.is_delete_user_popup_active}
                close_popup={() => {this.close_popup(utilities.keys_object.DELETE_USER)}}
                delete_users={this.delete_users}
                delete_status={users_state.status}
                delete_message={users_state.message}
                action_owner={users_state.action_owner}
            />
            <div className="row">
                <div className="twelve wide column">
                    <div className="ui fluid card">
                        <div className="extra content">
                            <div className="ui grid">
                                <div className="row">
                                    <div className="ten wide column">
                                        <PlainCircularBorderInputWithRightButton
                                            className="learnup-light-grey"
                                            placeholder={utilities.language.search}
                                            onClick={this.filter_users}
                                            alt_text={utilities.language.clear_result}
                                            alt_action={this.alt_filter_user_action}
                                        />
                                    </div>
                                    <div className="six wide column">
                                        <PlainDropDownWithCircularBorder
                                            option_list={users_state.roles_list}
                                            placeholder={utilities.language.choose_role}
                                            default_option={utilities.language.all}
                                            handle_option_change={this.filter_users_on_role}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="content users_list">
                            <Scrollbars>
                                <UnstackableVeryBasicStripedUsersTable
                                    rows={users_state.users_list}
                                    delete_user={this.open_delete_user_popup}
                                    edit_user={this.handle_edit_user}
                                />
                            </Scrollbars>
                        </div>
                        <div className="extra content table_extra_options">
                            <SimplePaginationMenu
                                total_number_of_pages={users_state.total_user_table_pages}
                                update_page_index={this.props.update_pagination_index}
                            />
                        </div>
                    </div>
                </div>
                <div className="three wide center aligned column action_cards">
                    <div className="row">
                        <div className="sixteen wide column">
                            <PlainCardWithImage
                                image={AddUserNewImage}
                                heading={utilities.language.add_users_text}
                                content={utilities.language.add_users_explained_text}
                                onClick={() => {this.open_popup(utilities.keys_object.ADD_USER)}}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="sixteen wide column">
                            <PlainCardWithImage
                                image={AddUserBulkImage}
                                heading={utilities.language.bulk_upload_users_text}
                                content={utilities.language.bulk_upload_users_explained_text}
                            />
                        </div>
                        <a id="download_template_link">{utilities.language.download_csv_template}</a>
                    </div>
                </div>
            </div>
        </div>;
    }

    componentWillUnmount() {
        this.props.reset_users_page_state();
    }

}

export default UsersPage;