/**
 * Created by anfal on 12/6/2016.
 */
import React from 'react';
import AuthorizationAwareComponent from '../../../security/authorization_components/authorization_aware_component';

class CourseSettingsPage extends AuthorizationAwareComponent {

    render() {
        return <h1>
            Course Settings
        </h1>
    }

}

export default CourseSettingsPage;