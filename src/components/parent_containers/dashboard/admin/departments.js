/**
 * Created by anfal on 1/4/2017.
 */

import React from 'react';
import AuthorizationAwareComponent from '../../../security/authorization_components/authorization_aware_component';
import ColoredCircularBorderButtonWithIcon from '../../../individual_components/buttons/colored_circular_border_button_with_icon';
import utilities from '../../../../controllers/utilities';
import UnstackableVeryBasicStripedDepartmentsTable from '../../../compound_components/tables/unstackable_very_basic_striped_departments_table';

class Departments extends AuthorizationAwareComponent {

    render() {

        return <div className="ui grid">
            <div className="row">
                <div className="twelve wide column">
                    <div className="ui grid">
                        <div className="row">
                            <div className="sixteen wide column">
                                <h3>{utilities.language.departments}</h3>
                            </div>
                        </div>
                        <div className="row">
                            <div className="sixteen wide column">
                                <UnstackableVeryBasicStripedDepartmentsTable/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="sixteen wide column">
                                <ColoredCircularBorderButtonWithIcon
                                    icon="plus"
                                    text={utilities.language.add_department}
                                    onClick={() => {}}
                                    color="green"
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="three wide column">

                </div>
            </div>
        </div>
    }

}

export default Departments;