/**
 * Created by anfal on 12/7/2016.
 */

import React, { Component } from 'react';
import TopMenuWithCenteredLogo from '../individual_components/menu/top_menu_with_centered_logo';
import FullPageLightGreyBackgroundDiv from '../individual_components/div/full_page_light_grey_background_div';
import PageNotFoundImage from '../../assets/images/page_not_found.png';
import utilities from '../../controllers/utilities';
import '../../assets/custom_css/not_found_page/not_found_page.css';
import BasicCircularBorderButton from '../individual_components/buttons/basic_circular_border_button';

class NotFoundPage extends Component {

    go_back_button_click_handler() {
        utilities.in_app_routings.redirectUserToDashboard();
    }

    render() {
        return <FullPageLightGreyBackgroundDiv>
            <div className="ui grid">
                <div className="row">
                    <div className="sixteen wide column">
                        <TopMenuWithCenteredLogo/>
                    </div>
                </div>
                <div className="row">
                    <div className="sixteen wide column not_found_page_centered_div">
                        <img
                            src={PageNotFoundImage}
                            alt={utilities.language.page_not_found}
                            className="image not_found_page_image"
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="sixteen wide column not_found_page_centered_div">
                        <BasicCircularBorderButton
                            color="black"
                            onClick={this.go_back_button_click_handler}
                            text={utilities.language.back_to_home}
                        />
                    </div>
                </div>
            </div>
        </FullPageLightGreyBackgroundDiv>
    }

}

export default NotFoundPage;