/**
 * Created by anfal on 11/28/2016.
 */

import React, { Component, PropTypes } from 'react';
import TopMenuWithCenteredLogo from '../individual_components/menu/top_menu_with_centered_logo';
import '../../assets/custom_css/sign_in_page/sign_in_page.css';
import BackgroundImage from '../../assets/images/sign_in_background.png';
import SignInPanel from '../compound_components/panels/sign_in_panel';
import FullScreenPageLoader from '../individual_components/loaders/full_screen_page_loader';
import FullPageLightGreyBackgroundDiv from '../individual_components/div/full_page_light_grey_background_div';
import utilities from '../../controllers/utilities';

class SignInPage extends Component {

    static propTypes = {
        initiate_sign_in: PropTypes.func.isRequired,
        children: PropTypes.node,
        auth_state: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.initiateSignIn = this.initiateSignIn.bind(this);
        this.view_to_be_rendered = this.view_to_be_rendered.bind(this);
    }

    render() {
        return this.view_to_be_rendered();
    }

    view_to_be_rendered() {

        const { children, auth_state, generic_state } = this.props;

        if(children) {
            return children
        } else {
            return <FullScreenPageLoader
                loading_text={utilities.language.signing_in_text}
                is_loader_active={generic_state.is_loader_active}
                is_dimmer_active={generic_state.is_dimmer_active}
            >
                <FullPageLightGreyBackgroundDiv>
                    <TopMenuWithCenteredLogo />
                    <SignInPanel
                        initiateSignIn={this.initiateSignIn}
                        error_message={auth_state.message}
                    />
                    <img src={BackgroundImage} alt="Background" id="background_image"/>
                </FullPageLightGreyBackgroundDiv>
            </FullScreenPageLoader>
        }
    }

    initiateSignIn(username, password, remember_me) {
        this.props.initiate_sign_in(username, password, remember_me);
    }
}

export default SignInPage;