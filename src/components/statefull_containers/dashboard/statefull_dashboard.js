/**
 * Created by anfal on 12/2/2016.
 */

import Dashboard from '../../parent_containers/dashboard/dashboard';
import { connect } from 'react-redux';
import { logout } from '../../../redux/middlewares/AuthMiddleware';

const StateFullDashboard = connect(
    function mapStateToProps(state) {
        return {
            auth_state: state.auth_reducer,
            dashboard_state: state.dashboard_reducer,
            generic_state: state.generic_reducer
        }
    },
    function mapDispatchToProps(dispatch) {
        return {
            logout_user: () => dispatch(logout())
        }
    }
)(Dashboard);

export default StateFullDashboard;