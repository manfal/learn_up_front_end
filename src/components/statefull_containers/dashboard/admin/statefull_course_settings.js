/**
 * Created by anfal on 12/7/2016.
 */

import { connect } from 'react-redux';
import CourseSettingsPage from '../../../parent_containers/dashboard/admin/course_settings_page';

const StateFullCourseSettingPage = connect(
    function mapStateToProps(state) {
        return {
            auth_state: state.auth_reducer
        }
    },
    function mapDispatchToProps(dispatch) {
        return {

        }
    }
)(CourseSettingsPage);

export default StateFullCourseSettingPage;