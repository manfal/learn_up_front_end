/**
 * Created by anfal on 1/4/2017.
 */

import { connect } from 'react-redux';
import Departments from '../../../parent_containers/dashboard/admin/departments';

const StateFullDepartments = connect(
    function mapStateToProps(state) {
        return {
            auth_state: state.auth_reducer
        }
    },
    function mapDispatchToProps(dispatch) {
        return {

        }
    }
)(Departments);

export default StateFullDepartments;