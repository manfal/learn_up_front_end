/**
 * Created by anfal on 12/10/2016.
 */

import { connect } from 'react-redux';
import UsersPage from '../../../parent_containers/dashboard/admin/users_page';
import {
    get_all_users,
    get_university_data,
    insert_new_user,
    delete_users,
    update_user_page_index,
    filter_users,
    filter_users_by_role,
    fetch_edit_user_data
} from '../../../../redux/middlewares/dashboard/admin/UsersMiddleware';
import {
    clear_status_and_message,
    reset_users_page_state,
    reset_edit_user_data
} from '../../../../redux/actions/dashboard/admin/UsersActions';
import { show_dimmer, hide_dimmer } from '../../../../redux/actions/GenericActions';

const StateFullUsersPage = connect(
    function mapStateToProps(state) {
        return {
            users_state: state.users_reducer,
            auth_state: state.auth_reducer
        }
    },
    function mapDispatchToProps(dispatch) {
        return {
            get_all_users: (current_page_index) => {dispatch(get_all_users(current_page_index))},
            show_dimmer: () => {dispatch(show_dimmer())},
            hide_dimmer: () => {dispatch(hide_dimmer())},
            get_university_data: () => {dispatch(get_university_data())},
            insert_new_user: (user_data_object, current_page_index) => {dispatch(insert_new_user(user_data_object, current_page_index))},
            delete_users: (user_ids, current_page_index) => {dispatch(delete_users(user_ids, current_page_index))},
            clear_status_and_message: () => {dispatch(clear_status_and_message())},
            update_pagination_index: (index) => {dispatch(update_user_page_index(index))},
            filter_users: (filter_user_text, current_page_index) => {dispatch(filter_users(filter_user_text, current_page_index))},
            filter_users_by_role: (current_page_index, role) => {dispatch(filter_users_by_role(current_page_index, role))},
            reset_users_page_state:() => {dispatch(reset_users_page_state())},
            fetch_edit_user_data: (user_id) => {dispatch(fetch_edit_user_data(user_id))},
            reset_edit_user_data: () => {dispatch(reset_edit_user_data())}
        }
    }
)(UsersPage);

export default StateFullUsersPage;