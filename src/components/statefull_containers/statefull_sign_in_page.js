/**
 * Created by anfal on 12/1/2016.
 */

import SignInPage from '../parent_containers/sign_in_page';
import { connect } from 'react-redux';
import { initiate_sign_in } from '../../redux/middlewares/AuthMiddleware';

const StateFullSignInPage = connect(
    function mapStateToProps(state) {
        return {
            auth_state: state.auth_reducer,
            generic_state: state.generic_reducer
        }
    },
    function mapDispatchToProps(dispatch) {
        return {
            initiate_sign_in: (username, password, remember_me) => dispatch(initiate_sign_in(username, password, remember_me))
        }
    }
)(SignInPage);

export default StateFullSignInPage;