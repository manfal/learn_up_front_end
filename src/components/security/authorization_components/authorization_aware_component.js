/**
 * Created by anfal on 12/31/2016.
 */

import { Component, PropTypes } from 'react';
import _ from 'lodash';
import utilities from '../../../controllers/utilities';

class AuthorizationAwareComponent extends Component {

    static propTypes = {
        routes: PropTypes.array.isRequired,
        auth_state: PropTypes.object.isRequired
    };

    componentWillMount() {
        //Call this method using super if inheriting child overrides it, otherwise
        //authorization won't happen.
        const { auth_state } = this.props;
        const authorized_roles = _.chain(this.props.routes)
            .filter(item => item.authorize)
            .map(item => item.authorize)
            .flattenDeep()
            .value();
        if(0 < authorized_roles.length) {
            if(-1 === _.indexOf(authorized_roles, auth_state.active_role)) {
                utilities.in_app_routings.redirectUserToNotFoundPage();
            }
        }
    }

}

export default AuthorizationAwareComponent;