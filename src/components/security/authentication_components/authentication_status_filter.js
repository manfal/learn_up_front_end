/**
 * Created by anfal on 12/3/2016.
 */
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import utilities from '../../../controllers/utilities';
import SignInController from '../../../controllers/AuthController';
import { assign_data_from_storage_to_state } from '../../../redux/actions/AuthActions';

function authentication_status_filter(ComposedComponent) {

    class AuthenticationStatusFilter extends Component {

        static propTypes = {
            location: PropTypes.object.isRequired,
            assign_auth_data_to_state: PropTypes.func.isRequired,
            is_logged_in: PropTypes.bool.isRequired
        };

        constructor(props) {
            super(props);
            this.current_route = this.props.location.pathname;
        }

        componentWillMount() {
            const { is_logged_in, assign_auth_data_to_state } = this.props;
            let auth_data = SignInController.verify_user_is_authenticated(is_logged_in);
            if(auth_data.is_logged_in) {
                if(null !== auth_data.data) {
                    assign_auth_data_to_state(auth_data.data);
                    if(utilities.in_app_routings.getLoginPageRoute() === this.current_route) {
                        utilities.in_app_routings.redirectUserToDashboard();
                    }
                }
            } else {
                utilities.in_app_routings.redirectUserToLoginPage();
            }
        }

        render() {
            return <ComposedComponent {...this.props}/>
        }
    }

    return connect(
        function mapStateToProps(state) {
            return { is_logged_in: state.auth_reducer.is_logged_in };
        },
        function mapDispatchToProps(dispatch) {
            return {
                assign_auth_data_to_state: (data_object) => dispatch(assign_data_from_storage_to_state(data_object))
            }
        }
    )(AuthenticationStatusFilter);
}

export default authentication_status_filter;