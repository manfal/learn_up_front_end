/**
 * Created by anfal on 12/9/2016.
 */

import React, {PropTypes} from 'react';
import '../../../assets/custom_css/headers/plain_full_width_top_header.css';

const plain_full_width_top_header = ( {children} ) => {

    return <div className="ui bf top menu plain_full_width_top_header">
        {children}
    </div>

};

plain_full_width_top_header.propTypes = {
    children: PropTypes.node
};

export default plain_full_width_top_header;
