/**
 * Created by anfal on 12/1/2016.
 */

import React, { PropTypes } from 'react';
import '../../../assets/custom_css/loaders/full_screen_page_loader.css';

const full_screen_page_loader = ({ is_loader_active, is_dimmer_active, loading_text, children }) => {
    let loaderUI = null;
    if(is_dimmer_active && !is_loader_active) {
        loaderUI = <div className="ui active dimmer"></div>
    } else if(!is_dimmer_active && is_loader_active) {
        loaderUI = <div className="ui active dimmer">
            <div className="ui large text loader">{loading_text}</div>
        </div>
    }

    return <div className="ui segment page_loader">
        {loaderUI}
        {children}
    </div>
};

full_screen_page_loader.propTypes = {
    is_loader_active: PropTypes.bool.isRequired,
    is_dimmer_active: PropTypes.bool.isRequired,
    loading_text: PropTypes.string,
    children: PropTypes.node.isRequired
};

export default full_screen_page_loader;