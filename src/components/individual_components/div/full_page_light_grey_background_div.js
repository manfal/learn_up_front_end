/**
 * Created by anfal on 12/9/2016.
 */

import React, { PropTypes } from 'react';
import '../../../assets/custom_css/div/full_page_light_grey_background_div.css';

const full_page_light_grey_background_div = ( {children} ) => {
    return <div className="full_page_light_grey_background_div">
        {children}
    </div>
};

full_page_light_grey_background_div.propTypes = {
    children: PropTypes.node.isRequired
};

export default full_page_light_grey_background_div;