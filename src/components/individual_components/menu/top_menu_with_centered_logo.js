/**
 * Created by anfal on 11/28/2016.
 */

import React from 'react';
import Logo from '../../../assets/images/logo.svg';
import '../../../assets/custom_css/menu/top_menu_with_centered_logo.css';
import PlainFullWidthTopHeader from '../headers/plain_full_width_top_header';

const top_menu_with_centered_logo = () => {
    return <PlainFullWidthTopHeader>
        <img src={Logo} className="top_logo" alt="LearnUp Logo"/>
    </PlainFullWidthTopHeader>
};

export default top_menu_with_centered_logo;