/**
 * Created by anfal on 12/5/2016.
 */

import React, { PropTypes } from 'react';
import '../../../assets/custom_css/menu/side_bar_menu.css';
import { Link } from 'react-router';
import { v4 } from 'node-uuid';

const side_bar_menu = ({ current_path, menu_items, set_currently_active_menu_item })  => {
    let menu = [];
    if(undefined !== menu_items) {
        menu = menu_items.map((menu_item) => {
            if(current_path === menu_item.url) {
                return <div className="active_menu_item" key={v4()}>
                    <span className="active_item_span"/>
                    <span className="active_item item active">
                        {menu_item.name}
                    </span>
                </div>
            } else {
                return <Link
                    to={menu_item.url}
                    className="item menu_item"
                    key={v4()}
                    onClick={() => {set_currently_active_menu_item(menu_item.url)}}
                >
                    {menu_item.name}
                </Link>
            }
        });
    }

    return <div className="vertical_menu_div">
        <div className="ui grid">
            <div className="ui vertical fluid tabular menu vertical_menu">
                {menu}
            </div>
        </div>
    </div>
};

side_bar_menu.propTypes = {
    current_path: PropTypes.string.isRequired,
    menu_items: PropTypes.array,
    set_currently_active_menu_item: PropTypes.func.isRequired
};

export default side_bar_menu;