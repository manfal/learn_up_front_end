/**
 * Created by anfal on 12/26/2016.
 */
import React, { PropTypes } from 'react';

const plain_right_floated_checkbox = ({ checked_state, index, label, handle_checked_state_change, className }) => {
    let checkbox = null;

    if(undefined !== index) {
        checkbox = <input
            type="checkbox"
            checked={checked_state}
            onChange={() => {handle_checked_state_change(index)}}
        />;
    } else {
        checkbox = <input
            type="checkbox"
            checked={checked_state}
            onChange={handle_checked_state_change}
        />;
    }

    return <div className={ "ui right floated checkbox " + className }>
        {checkbox}
        <label>{label}</label>
    </div>
};

plain_right_floated_checkbox.propTypes = {
    label: PropTypes.string,
    index: PropTypes.number,
    checked_state: PropTypes.bool.isRequired,
    handle_checked_state_change: PropTypes.func.isRequired,
    className: PropTypes.string
};

export default plain_right_floated_checkbox;