/**
 * Created by anfal on 1/1/2017.
 */

import React, { PropTypes } from 'react';
import '../../../assets/custom_css/buttons/basic_circular_border_button.css';

const basic_circular_border_button = ({ color, onClick, text }) => {

    return <button
        className={"ui " + color + " basic button basic_circular_border_button"}
        onClick={onClick}
    >
        {text}
    </button>

};

basic_circular_border_button.propTypes = {
    color: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired
};

basic_circular_border_button.defaultProps = {
    color: 'primary'
};

export default basic_circular_border_button;