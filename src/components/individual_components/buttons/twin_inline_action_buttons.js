/**
 * Created by anfal on 12/13/2016.
 */

import React, { PropTypes } from 'react';

const twin_inline_action_buttons = ({
    button_one_color,
    button_two_color,
    handle_button_one_click,
    handle_button_two_click,
    button_one_text,
    button_two_text
}) => {
    return <div>
        <button className={"ui " + button_one_color + " button"} onClick={handle_button_one_click}>
            { button_one_text }
        </button>
        <button className={"ui " + button_two_color + " button"} onClick={handle_button_two_click}>
            { button_two_text }
        </button>
    </div>
};

twin_inline_action_buttons.propTypes = {
    button_one_color: PropTypes.string,
    button_two_color: PropTypes.string,
    handle_button_one_click: PropTypes.func.isRequired,
    handle_button_two_click: PropTypes.func.isRequired,
    button_one_text: PropTypes.string.isRequired,
    button_two_text: PropTypes.string.isRequired
};

twin_inline_action_buttons.defaultProps = {
    button_one_color: "primary",
    button_two_color: "primary"
};

export default twin_inline_action_buttons;