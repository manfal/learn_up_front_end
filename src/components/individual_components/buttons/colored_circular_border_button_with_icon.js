/**
 * Created by anfal on 1/5/2017.
 */

import React, { PropTypes } from 'react';
import '../../../assets/custom_css/buttons/colored_circular_border_button_with_icon.css';

const colored_circular_border_button_with_icon = ({ icon, text, color, onClick }) => {
    return <button
        className={"ui "+ color +" button colored_circular_border_button_with_icon"}
        onClick={onClick}
    >
        <i className={icon + " icon"}/>
        {text}
    </button>
};

colored_circular_border_button_with_icon.propTypes = {
    icon: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    color: PropTypes.string.isRequired
};

export default colored_circular_border_button_with_icon;