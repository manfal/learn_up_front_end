/**
 * Created by anfal on 12/9/2016.
 */

import React, { PropTypes } from 'react';
import '../../../assets/custom_css/buttons/plain_rectangular_button.css';

const plain_rectangular_button = ({ color, text, handleClick }) => {

    return <div className="plain_rectangular_button">
        <button className={"ui "+ color +" button"} onClick={handleClick}>
            {text}
        </button>
    </div>

};

plain_rectangular_button.propTypes = {
    color: PropTypes.string,
    handleClick: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired
};

plain_rectangular_button.defaultProps = {
    color: "default"
};

export default plain_rectangular_button;