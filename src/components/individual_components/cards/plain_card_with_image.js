/**
 * Created by anfal on 12/10/2016.
 */

import React, { PropTypes } from 'react';
import DefaultImage from '../../../assets/images/default_image.png';
import '../../../assets/custom_css/cards/plain_card_with_image.css';

const plain_card_with_image = ( {image, heading, content, onClick } ) => {
    return <div className="ui fluid card plain_card_with_image" onClick={onClick}>
        <div className="image plain_card_with_image_image_parent">
            <img src={image} alt={heading}/>
        </div>
        <div className="content plain_card_with_image_content_parent">
            <h3>{heading}</h3>
            <p id="plain_card_with_image_content">{content}</p>
        </div>
    </div>
};

plain_card_with_image.propTypes = {
    image: PropTypes.string.isRequired,
    heading: PropTypes.string,
    content: PropTypes.string,
    onClick: PropTypes.func
};

plain_card_with_image.defaultProps = {
    image: DefaultImage
};

export default plain_card_with_image;