/**
 * Created by anfal on 12/2/2016.
 */

import React, { PropTypes } from 'react';
import '../../../assets/custom_css/messages/simple_div_based_message.css';

const simple_div_based_message = ({ message, message_type }) => {
    if("" !== message) {
        return <div className={"ui "+ message_type +" message simple_message"}>
            {message}
        </div>
    } else {
        return <div></div>
    }
};

simple_div_based_message.propTypes = {
    message: PropTypes.string.isRequired,
    message_type: PropTypes.string
};

simple_div_based_message.defaultProps = {
    message_type: "default"
};

export default simple_div_based_message;