/**
 * Created by anfal on 12/16/2016.
 */

import React, { PropTypes } from 'react';

const field_input = (props) => {
    return <div className={"field " + props.alert_class}>
        <label>{props.label}</label>
        <input
            type={props.type}
            placeholder={props.placeholder}
            value={props.value}
            onChange={props.onChange}
            data-key={props.data_key}
        />
    </div>
};

field_input.propTypes = {
    alert_class: PropTypes.string,
    label: PropTypes.string.isRequired,
    type: PropTypes.string,
    placeholder: PropTypes.string,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    data_key: PropTypes.string.isRequired
};

field_input.defaultProps = {
    type: "text"
};

export default field_input;