/**
 * Created by anfal on 12/13/2016.
 */

import React, { Component, PropTypes } from 'react';
import PlainDropDownWithRectangularBorder from '../../../individual_components/dropdowns/plain_drop_down_wth_rectangular_border';

class SimpleDropdown extends Component {

    static propTypes = {
        option_list: PropTypes.array.isRequired,
        value: PropTypes.any,
        onChange: PropTypes.func,
        drop_down_label: PropTypes.string,
        data_key: PropTypes.string.isRequired
    };

    constructor(props) {
        super(props);
        this.get_drop_down_items = this.get_drop_down_items.bind(this);
        this.handleDropDownOptionChange = this.handleDropDownOptionChange.bind(this);
    }

    get_drop_down_items() {
        const { option_list } = this.props;
        return option_list.map(option_item => {
            if("object" === typeof option_item) {
                return option_item[1];
            }
            return option_item;
        });
    }

    handleDropDownOptionChange(text) {
        this.props.onChange({
            key: this.props.data_key,
            value: text
        });
    }

    render() {

        const { drop_down_label } = this.props;

        return <div className="field">
            <label>{drop_down_label}</label>
            <PlainDropDownWithRectangularBorder
                option_list={this.get_drop_down_items()}
                placeholder=""
                handle_option_change={this.handleDropDownOptionChange}
            />
        </div>
    }

}

export default SimpleDropdown;