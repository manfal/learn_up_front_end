/**
 * Created by anfal on 1/1/2017.
 */

import React, { Component, PropTypes } from 'react';
import { v4 } from 'node-uuid';
import _ from 'lodash';
import '../../../assets/custom_css/dropdowns/plain_dropdown_with_rectangular_border.css';

class PlainDropDownWithRectangularBorder extends Component {

    static propTypes = {
        option_list: PropTypes.array.isRequired,
        placeholder: PropTypes.string.isRequired,
        default_option: PropTypes.string,
        handle_option_change: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);
        this.handleDropDownClick = this.handleDropDownClick.bind(this);
        this.handleItemClick = this.handleItemClick.bind(this);
        this.getDropDownList = this.getDropDownList.bind(this);

        this.state = {
            current_drop_down_value: this.props.placeholder,
            is_drop_down_open: false,
            drop_down_active_class: '',
            drop_down_items_active_class: ''
        }
    }

    handleDropDownClick() {
        let new_drop_down_value = !this.state.is_drop_down_open;
        if (new_drop_down_value) {
            this.setState({
                drop_down_active_class: 'active visible',
                drop_down_items_active_class: 'transition visible',
                is_drop_down_open: new_drop_down_value
            });
        } else {
            this.setState({
                drop_down_active_class: '',
                drop_down_items_active_class: '',
                is_drop_down_open: new_drop_down_value
            });
        }
    }

    handleItemClick(event) {
        let text = event.target.innerText;
        this.setState({
            current_drop_down_value: text
        });
        this.props.handle_option_change(text);
    }

    getDropDownList() {
        const { option_list, default_option } = this.props;
        let options = option_list.map(option => {
            return <div className="item" onClick={this.handleItemClick} key={v4()}>{option}</div>
        });
        if(undefined !== default_option && '' !== default_option) {
            return _.concat(
                <div className="item" onClick={this.handleItemClick} key={v4()}>{default_option}</div>,
                options
            );
        }
        return options;
    }

    render() {
        return <div
            className={"ui selection dropdown "+this.state.drop_down_active_class}
            onClick={this.handleDropDownClick}
        >
            <i className="dropdown icon"/>
            <div
                className="default text plain_dropdown_with_rectangular_border_default_text">
                {this.state.current_drop_down_value}
            </div>
            <div className={"menu "+this.state.drop_down_items_active_class}>
                {this.getDropDownList()}
            </div>
        </div>
    }

}

export default PlainDropDownWithRectangularBorder;