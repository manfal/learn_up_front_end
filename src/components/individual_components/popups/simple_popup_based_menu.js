/**
 * Created by anfal on 12/27/2016.
 */
import React, { PropTypes } from 'react';
import { v4 } from 'node-uuid';

const simple_popup_based_menu = ({
    direction,
    is_popup_active,
    icon,
    index,
    handle_open_menu_click,
    label,
    items,
    unique_action_identifier
}) => {
    let drop_down_class = (is_popup_active) ? "active" : "";
    let menu_class = (is_popup_active) ? "transition visible" : "";
    let popup_items = items.map((item) => {
        return <div
            className="item"
            key={v4()}
            onClick={() => {item.action(unique_action_identifier)}}>
            {item.name}
        </div>
    });

    return <div
        className={"ui "+ direction +" pointing dropdown icon " + drop_down_class}
        onClick={() => handle_open_menu_click(index)}>
        <i className={icon + " icon"}/>
        <div className={"menu " + menu_class}>
            {popup_items}
        </div>
        <label>{label}</label>
    </div>
};

simple_popup_based_menu.propTypes = {
    direction: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    is_popup_active: PropTypes.bool.isRequired,
    unique_action_identifier: PropTypes.any.isRequired,
    items: PropTypes.array.isRequired,
    handle_open_menu_click: PropTypes.func.isRequired,
    index: PropTypes.number.isRequired,
    label: PropTypes.string
};

export default simple_popup_based_menu;