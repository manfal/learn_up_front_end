/**
 * Created by anfal on 12/20/2016.
 */

import React, { Component, PropTypes } from 'react';
import '../../../assets/custom_css/pagination/simple_paginaton_menu.css';
import _ from 'lodash';
import { v4 } from 'node-uuid';

class SimplePaginationMenu extends Component {

    static propTypes = {
        total_number_of_pages: PropTypes.number.isRequired,
        update_page_index: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.get_pagination_list = this.get_pagination_list.bind(this);
        this.get_compressed_list = this.get_compressed_list.bind(this);
        this.handle_chevron_click = this.handle_chevron_click.bind(this);
        this.state = {
            currently_active_page_index: 1
        };
        this.chevron_direction = {
            RIGHT: 'right',
            LEFT: 'left'
        };
    }

    get_pagination_list() {
        const { total_number_of_pages } = this.props;
        let compression_threshold = 5;
        if(total_number_of_pages > compression_threshold) {
            return this.get_compressed_list(total_number_of_pages);
        } else {
            return this.get_un_compressed_list(0, total_number_of_pages);
        }
    }

    get_un_compressed_list(start_index, number_of_pages) {
        return _.range(start_index, number_of_pages, 1).map(index => {
            let original_index = index + 1;
            if(this.state.currently_active_page_index === original_index) {
                return <span className="item active_pagination_item" key={v4()}>
                    {original_index}
                </span>
            }
            return <a
                className="item inactive_pagination_item"
                key={v4()}
                onClick={() => this.handle_pagination_item_click(original_index)}>
                {original_index}
            </a>
        });
    }

    get_compressed_list(number_of_pages) {
        const { currently_active_page_index } = this.state;
        let max_compressed_list_size = 5;
        let start_index = currently_active_page_index - 1;
        let end_index = (currently_active_page_index + max_compressed_list_size) - 1;

        if(start_index > (number_of_pages - max_compressed_list_size)) {
            start_index = (number_of_pages - max_compressed_list_size);
        }

        if(end_index > number_of_pages) {
            end_index = number_of_pages
        }

        let uncompressed_array = this.get_un_compressed_list(0, number_of_pages);

        if(start_index === (number_of_pages - max_compressed_list_size) && end_index === number_of_pages) {
            return _.concat(
                <a
                    className="icon item pagination_chevron"
                    onClick={() => this.handle_chevron_click(this.chevron_direction.LEFT)}
                >
                    <i className="left chevron icon"/>
                </a>,
                uncompressed_array[0],
                <span className="item" key={v4()}>
                    ...
                </span>,
                uncompressed_array.slice(start_index, end_index),
                <a
                    className="icon item pagination_chevron"
                    onClick={() => this.handle_chevron_click(this.chevron_direction.RIGHT)}
                >
                    <i className="right chevron icon"/>
                </a>
            );
        } else {
            return _.concat(
                <a
                    className="icon item pagination_chevron"
                    onClick={() => this.handle_chevron_click(this.chevron_direction.LEFT)}
                >
                    <i className="left chevron icon"/>
                </a>,
                uncompressed_array.slice(start_index, end_index),
                <span className="item" key={v4()}>
                    ...
                </span>,
                uncompressed_array[number_of_pages -1],
                <a
                    className="icon item pagination_chevron"
                    onClick={() => this.handle_chevron_click(this.chevron_direction.RIGHT)}
                >
                    <i className="right chevron icon"/>
                </a>
            );
        }
    }

    handle_pagination_item_click(index) {
        this.setState({
            currently_active_page_index: index
        });
        this.props.update_page_index(index);
    }

    handle_chevron_click(chevron_direction) {
        switch(chevron_direction) {
            case this.chevron_direction.LEFT:
                let dec_index = this.state.currently_active_page_index - 1;
                if(0 < dec_index) {
                    this.handle_pagination_item_click(dec_index);
                }
                break;
            case this.chevron_direction.RIGHT:
                let inc_index = this.state.currently_active_page_index + 1;
                if(this.props.total_number_of_pages >= inc_index) {
                    this.handle_pagination_item_click(inc_index);
                }
                break;
            default:
                break;
        }
    }

    render() {
        return <div className="ui pagination menu simple_pagination_menu">
            {this.get_pagination_list()}
        </div>
    }

}


export default SimplePaginationMenu;