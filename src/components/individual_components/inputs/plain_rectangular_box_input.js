/**
 * Created by anfal on 12/9/2016.
 */

import React, { Component, PropTypes } from 'react';
import '../../../assets/custom_css/inputs/plain_rectangular_box_input.css';

class PlainRectangularBoxInput extends Component {

    static propTypes = {
        type: PropTypes.string.isRequired,
        placeholder: PropTypes.string,
        className: PropTypes.string,
        onChange: PropTypes.func.isRequired
    };

    static defaultProps = {
        type: "text"
    };

    constructor(props) {
        super(props);
        this.state = {
            input_value: ''
        };
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    render() {
        const { placeholder, type, className } = this.props;

        return <div className={"ui input " + className + " input_field"}>
            <input
                type={type}
                placeholder={placeholder}
                value={this.state.input_value}
                onChange={this.handleInputChange}
            />
        </div>
    }

    handleInputChange(event) {
        let new_value = event.target.value;
        this.setState({
            input_value: new_value
        });
        this.props.onChange(new_value);
    }

}

export default PlainRectangularBoxInput;