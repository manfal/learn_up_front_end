/**
 * Created by anfal on 12/24/2016.
 */

import React, { Component, PropTypes } from 'react';
import '../../../assets/custom_css/inputs/plain_circular_border_input_with_right_button.css';
import utilities from '../../../controllers/utilities';

class PlainCircularBorderInputWithRightButton extends Component {

    static propTypes = {
        placeholder: PropTypes.string,
        type: PropTypes.string.isRequired,
        onClick: PropTypes.func.isRequired,
        className: PropTypes.string,
        alt_text: PropTypes.string.isRequired,
        alt_action: PropTypes.func.isRequired
    };

    static defaultProps = {
        type: 'text'
    };

    constructor(props) {
        super(props);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);

        this.initial_state = {
            input_value: '',
            button_text: this.props.placeholder
        };

        this.state = this.initial_state;
    }

    handleInputChange(event) {
        this.setState({input_value: event.target.value});
    }

    handleClick() {
        if(this.state.button_text === this.props.placeholder) {
            if('' !== this.state.input_value) {
                this.setState({
                    button_text: this.props.alt_text
                });
                this.props.onClick(this.state.input_value);
            }
        } else {
            this.setState(this.initial_state);
            this.props.alt_action();
        }
    }

    handleKeyPress(event) {
        if(utilities.keys_object.ENTER_KEY === event.which) {
            this.setState({
                button_text: this.props.alt_text
            });
            this.props.onClick(this.state.input_value);
        }
    }

    render() {

        const { placeholder, type, className} = this.props;

        return <div className="ui fluid action input plain_circular_border_input_with_right_button">
                <input
                    type={type}
                    placeholder={placeholder}
                    id="plain_circular_border_input_with_right_button_input"
                    className={className}
                    value={this.state.input_value}
                    onChange={this.handleInputChange}
                    onKeyPress={this.handleKeyPress}
                />
                <div
                    className="ui button"
                    id="plain_circular_border_input_with_right_button_button"
                    onClick={this.handleClick}
                >
                    {this.state.button_text}
                </div>
        </div>
    }

}

export default PlainCircularBorderInputWithRightButton;