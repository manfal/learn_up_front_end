/**
 * Created by anfal on 12/16/2016.
 */
import React, { Component, PropTypes } from 'react';
import TwinInlineActionButtons from '../../individual_components/buttons/twin_inline_action_buttons';
import '../../../assets/custom_css/modal/delete_a_user.css';
import SimpleDivBasedMessage from '../../individual_components/messages/simple_div_based_message';
import utilities from '../../../controllers/utilities';

class DeleteUserPopup extends Component {

    static propTypes = {
        is_popup_active: PropTypes.bool.isRequired,
        close_popup: PropTypes.func,
        delete_users: PropTypes.func.isRequired,
        delete_message: PropTypes.string.isRequired,
        delete_status: PropTypes.string.isRequired,
        action_owner: PropTypes.string.isRequired
    };

    constructor(props) {
        super(props);
        this.delete_popup_state = {
            message: ''
        };
        this.state = this.delete_popup_state;
        this.close_popup = this.close_popup.bind(this);
    }

    componentWillReceiveProps(props) {
        const { delete_message, delete_status, action_owner } = props;

        if(
            '' !== delete_message &&
            utilities.keys_object.FAIL === delete_status &&
            utilities.keys_object.DELETE_USER === action_owner
        ) {
            this.setState({
                message: delete_message
            });
        } else if('' === delete_message && utilities.keys_object.SUCCESS === delete_status) {
            this.close_popup();
        }
    }

    close_popup() {
        this.setState(this.delete_popup_state);
        this.props.close_popup();
    }

    render() {
        const { is_popup_active, delete_users } = this.props;
        let popup_active_class = (is_popup_active) ? "active" : "";

        return <div className={"ui " + popup_active_class + " small modal delete_a_user_modal"}>
            <div className="header">{utilities.language.delete_user_text}</div>
            <div className="content">
                <SimpleDivBasedMessage
                    message={this.state.message}
                />
                {utilities.language.confirm_delete_user}
            </div>
            <div className="actions">
                <div className="ui grid">
                    <div className="row action_buttons_div">
                        <div className="six wide right floated column">
                            <TwinInlineActionButtons
                                button_one_color="red"
                                button_two_color="green"
                                handle_button_one_click={this.close_popup}
                                handle_button_two_click={delete_users}
                                button_one_text={utilities.language.no}
                                button_two_text={utilities.language.yes}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }

}

export  default DeleteUserPopup;