/**
 * Created by anfal on 12/12/2016.
 */

import React, { Component, PropTypes } from 'react';
import '../../../assets/custom_css/modal/add_new_user_popup.css';
import TwinInlineActionButtons from '../../individual_components/buttons/twin_inline_action_buttons';
import SimpleDropdown from '../../individual_components/form/dropdowns/simple_dropdown';
import utilities from '../../../controllers/utilities';
import FieldInput from '../../individual_components/form/field/field_input';
import SimpleDivBasedMessage from '../../individual_components/messages/simple_div_based_message';

class CreateUpdateUserModal extends Component {

    static propTypes = {
        is_popup_active: PropTypes.bool.isRequired,
        close_popup: PropTypes.func,
        roles_list: PropTypes.array.isRequired,
        batch_list: PropTypes.array.isRequired,
        department_list: PropTypes.array.isRequired,
        add_new_user: PropTypes.func.isRequired,
        insert_message: PropTypes.string.isRequired,
        insert_status: PropTypes.string.isRequired,
        action_owner: PropTypes.string.isRequired
    };

    constructor(props) {
        super(props);
        this.state_object = {
            first_name_class: "",
            last_name_class: "",
            user_name_class: "",
            email_class: "",
            first_name: "",
            last_name: "",
            email: "",
            username: "",
            role: "",
            batch: "",
            department: "",
            message: ""
        };
        this.state = this.state_object;
        this.add_new_user = this.add_new_user.bind(this);
        this.close_popup = this.close_popup.bind(this);
        this.update_field_input = this.update_field_input.bind(this);
        this.update_dropdown_value = this.update_dropdown_value.bind(this);
    }

    componentWillMount() {

        const { batch_list, roles_list, department_list }= this.props;

        if(
            batch_list[0] !== this.state.batch && batch_list[0] !== undefined &&
            roles_list[0] !== this.state.role && roles_list[0] !== undefined &&
            department_list[0] !== this.state.department && department_list[0] !== undefined
        ) {
            this.setState({
                batch: batch_list[0],
                role: roles_list[0],
                department: department_list[0][0]
            });
        }
    }

    componentWillReceiveProps(props) {
        const { insert_status, insert_message, action_owner } = props;

        if (
            '' !== insert_message &&
            utilities.keys_object.FAIL === insert_status &&
            utilities.keys_object.CREATE_OR_UPDATE_USER === action_owner
        ) {
            this.setState({
                message: insert_message
            });
        } else if ('' === insert_message && utilities.keys_object.SUCCESS === insert_status) {
            this.close_popup();
        }
    }

    update_field_input(event) {
        this.setState({ [event.target.dataset.key] : event.target.value });
    }

    update_dropdown_value(value_object) {
        this.setState({ [value_object.key] : value_object.value });
    }

    add_new_user() {
        const { first_name, last_name, email, username, role, batch, department } = this.state;
        if("" === first_name) {
            this.setState({
               first_name_class: "error"
            });
        } else if("" === last_name) {
            this.setState({
                last_name_class: "error"
            });
        } else if("" === email) {
            this.setState({
                email_class: "error"
            });
        } else if("" === username) {
            this.setState({
                user_name_class: "error"
            });
        } else {
            let roles = [role];
            let user_data_object = {
                first_name: first_name,
                last_name: last_name,
                username: username,
                email: email,
                batch: batch,
                roles: roles,
                department: department
            };
            this.props.add_new_user(user_data_object);
        }
    }

    close_popup() {
        this.setState(this.state_object);
        this.props.close_popup();
    };

    render() {

        const { is_popup_active, roles_list, batch_list, department_list } = this.props;
        let popup_active_class = (is_popup_active) ? "active" : "";

        return <div className={"ui " + popup_active_class + " modal add_new_user_modal"}>
            <div className="header">{utilities.language.add_new_user_text}</div>
            <div className="content">
                <div className="ui form error">
                    <SimpleDivBasedMessage
                        message={this.state.message}
                    />
                    <SimpleDropdown
                        option_list={roles_list}
                        drop_down_label={utilities.language.choose_a_role}
                        onChange={this.update_dropdown_value}
                        data_key="role"
                    />
                    <div className="two fields">
                        <FieldInput
                            alert_class={this.state.first_name_class}
                            label={utilities.language.first_name}
                            placeholder={utilities.language.first_name}
                            value={this.state.first_name}
                            data_key="first_name"
                            onChange={this.update_field_input}
                        />
                        <FieldInput
                            alert_class={this.state.last_name_class}
                            label={utilities.language.last_name}
                            placeholder={utilities.language.last_name}
                            value={this.state.last_name}
                            data_key="last_name"
                            onChange={this.update_field_input}
                        />
                    </div>
                    <div className="two fields">
                        <FieldInput
                            alert_class={this.state.user_name_class}
                            label={utilities.language.username}
                            placeholder={utilities.language.username}
                            value={this.state.username}
                            data_key="username"
                            onChange={this.update_field_input}
                        />
                        <FieldInput
                            alert_class={this.state.email_class}
                            label={utilities.language.email}
                            type="email"
                            placeholder={utilities.language.email}
                            value={this.state.email}
                            data_key="email"
                            onChange={this.update_field_input}
                        />
                    </div>
                    <div className="two fields">
                        <SimpleDropdown
                            option_list={batch_list}
                            drop_down_label={utilities.language.choose_a_batch}
                            onChange={this.update_dropdown_value}
                            data_key="batch"
                        />
                        <SimpleDropdown
                            option_list={department_list}
                            drop_down_label={utilities.language.choose_a_department}
                            onChange={this.update_dropdown_value}
                            data_key="department"
                        />
                    </div>
                </div>
            </div>
            <div className="actions">
                <div className="ui grid">
                    <div className="row action_buttons_div">
                        <div className="six wide right floated column">
                            <TwinInlineActionButtons
                                button_one_color="red"
                                button_two_color="green"
                                handle_button_one_click={this.close_popup}
                                handle_button_two_click={this.add_new_user}
                                button_one_text={utilities.language.cancel_text}
                                button_two_text={utilities.language.add_text}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }

}

export default CreateUpdateUserModal;