/**
 * Created by anfal on 12/3/2016.
 */

import React, { Component, PropTypes } from 'react';
import Logo from '../../../assets/images/logo.svg';
import '../../../assets/custom_css/menu/top_menu_with_left_logo.css';
import DefaultAvatar from '../../../assets/images/default_image.png';
import PlainFullWidthTopHeader from '../../individual_components/headers/plain_full_width_top_header';

class DashboardTopMenu extends Component {

    static propTypes = {
        logout_user: PropTypes.func.isRequired,
        first_name: PropTypes.string,
        last_name: PropTypes.string
    };

    constructor(props) {
        super(props);
        this.handleLogOutClick = this.handleLogOutClick.bind(this);
    }

    handleLogOutClick() {
        this.props.logout_user();
    }

    render() {

        const { first_name, last_name } = this.props;

        return <PlainFullWidthTopHeader>
            <div className="ui grid top_menu_with_left_logo">
                <div className="row">
                    <div className="three wide column">
                        <img src={Logo} className="image dashboard_top_logo" alt="LearnUp Logo"/>
                    </div>
                    <div className="thirteen wide column">
                        <div className="ui compact menu dashboard_top_menu_options_right">
                            <div className="ui simple item">
                                <i className="alarm outline icon notification_icon"/>
                            </div>
                            <div className="ui simple dropdown item">
                                <img className="ui avatar image" src={DefaultAvatar} alt="User Avatar"/>
                                <span>{first_name + " " + last_name}</span>
                                <i className="dropdown icon"/>
                                <div className="menu">
                                    <div className="item" onClick={this.handleLogOutClick}>Logout</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </PlainFullWidthTopHeader>
    }
}
export default DashboardTopMenu;