/**
 * Created by anfal on 11/28/2016.
 */

import React, { Component, PropTypes } from 'react';
import '../../../assets/custom_css/sign_in_page/sign_in_panel.css';
import SimpleDivBasedMessage from '../../individual_components/messages/simple_div_based_message';
import utilities from '../../../controllers/utilities';
import PlainRectangularBoxInput from '../../individual_components/inputs/plain_rectangular_box_input';
import PlainRectangularButton from '../../individual_components/buttons/plain_rectangular_button';
import PlainRightFloatedCheckbox from '../../individual_components/checkbox/plain_right_floated_checkbox';

class SignInPanel extends Component {

    static propTypes = {
        initiateSignIn: PropTypes.func.isRequired,
        error_message: PropTypes.string.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            remember_me_check_box: false,
            username: '',
            password: '',
            username_error_class: '',
            password_error_class: ''
        };
        this.changeCheckboxState = this.changeCheckboxState.bind(this);
        this.handleSignInClick = this.handleSignInClick.bind(this);
        this.updateUsernameState = this.updateUsernameState.bind(this);
        this.updatePasswordState = this.updatePasswordState.bind(this);
    }

    changeCheckboxState() {
        this.setState({
            remember_me_check_box: !this.state.remember_me_check_box
        });
    }

    updateUsernameState(value) {
        this.setState({
            username: value
        });
    }

    updatePasswordState(value) {
        this.setState({
            password: value
        });
    }

    handleSignInClick() {
        if('' === this.state.username) {
            this.setState({
                username_error_class: 'error'
            });
        } else if('' === this.state.password) {
            this.setState({
                password_error_class: 'error'
            });
        } else {
            this.setState({
                username_error_class: '',
                password_error_class: ''
            });
            this.props.initiateSignIn(
                this.state.username,
                this.state.password,
                true
            );
        }
    }

    render() {

        const { error_message } = this.props;

        return <div className="ui card sign_in_card">
            <SimpleDivBasedMessage
                message_type="error"
                message={error_message}
            />

            <PlainRectangularBoxInput
                className={this.state.username_error_class}
                type="text"
                placeholder={utilities.language.email_or_username_text}
                onChange={this.updateUsernameState}
            />

            <PlainRectangularBoxInput
                className={this.state.password_error_class}
                type="password"
                placeholder={utilities.language.password_text}
                onChange={this.updatePasswordState}
            />

            <PlainRightFloatedCheckbox
                checked_state={this.state.remember_me_check_box}
                handle_checked_state_change={this.changeCheckboxState}
                label={utilities.language.remember_me}
                className="remember_me_checkbox"
            />

            <PlainRectangularButton
                color="green"
                text={utilities.language.sign_in_text}
                handleClick={this.handleSignInClick}
            />

        </div>
    }
}

export default SignInPanel;