/**
 * Created by anfal on 12/11/2016.
 */

import React, { Component, PropTypes } from 'react';
import '../../../assets/custom_css/tables/unstackable_very_basic_striped_users_table.css';
import PlainRightFloatedCheckbox from '../../individual_components/checkbox/plain_right_floated_checkbox';
import { v4 } from 'node-uuid';
import utilities from '../../../controllers/utilities';
import SimplePopupBasedMenu from '../../individual_components/popups/simple_popup_based_menu';

class UnstackableVeryBasicStripedUsersTable extends Component {

    static propTypes = {
        rows: PropTypes.array.isRequired,
        delete_user: PropTypes.func.isRequired,
        edit_user: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.getTableRows = this.getTableRows.bind(this);
        this.open_setting_menu = this.open_setting_menu.bind(this);
        this.handle_user_action_checkbox_state = this.handle_user_action_checkbox_state.bind(this);
        this.handle_check_all_check_box_click = this.handle_check_all_check_box_click.bind(this);
        this.get_new_checkbox_state = this.get_new_checkbox_state.bind(this);
        this.get_action_table_header = this.get_action_table_header.bind(this);
        this.handleDeleteClick = this.handleDeleteClick.bind(this);
        this.handleEditClick = this.handleEditClick.bind(this);

        this.state = {
            selected_row_index: 0,
            is_action_menu_active: false,
            action_checkbox_state: [],
            action_check_all_checkbox_state: false,
            is_bulk_action_popup_open: false
        };

        this.pop_menu_items = [
            {
                name: utilities.language.delete,
                action: this.handleDeleteClick
            },
            {
                name: utilities.language.edit,
                action: this.handleEditClick
            }
        ];

        //-2 is an indicator for check all boxes, gave it -2 index because
        // it is highly unlikely anyone else will get it
        this.bulk_delete_index = -2;
    }

    componentWillReceiveProps(props) {
        const { rows } = props;
        if(rows !== this.props.rows) {
            let user_action_checkbox_state = rows.map((row) => {
                return {
                    key: row.key,
                    is_checked: false
                };
            });
            this.setState({
                action_checkbox_state: user_action_checkbox_state
            });
        }
    }

    handleDeleteClick(user_id) {
        const { delete_user } = this.props;
        if(this.bulk_delete_index === user_id) {
            let bulk_delete_list = [];
            for(let checkbox_state of this.state.action_checkbox_state) {
                if(checkbox_state.is_checked) {
                    bulk_delete_list.push(checkbox_state.key);
                }
            }
            delete_user({
                user_ids: bulk_delete_list
            });
        } else {
            delete_user({
                user_ids: [
                    user_id
                ]
            });
        }
    }

    handleEditClick(user_id) {
        this.props.edit_user(user_id);
    }

    handle_user_action_checkbox_state(index) {
        this.setState({
            action_checkbox_state: this.get_new_checkbox_state(index)
        });
    }

    get_new_checkbox_state(index) {
        let new_state = [];
        let i = 0;
        for(let old_state of this.state.action_checkbox_state) {
            let state_object = {
                key: old_state.key,
                is_checked: old_state.is_checked
            };
            if(i === index || this.bulk_delete_index === index) {
                state_object.is_checked = !old_state.is_checked;
            }
            new_state.push(state_object);
            i++;
        }
        return new_state;
    }

    handle_check_all_check_box_click(index) {
        this.setState({
            action_check_all_checkbox_state: !this.state.action_check_all_checkbox_state,
            action_checkbox_state: this.get_new_checkbox_state(index)
        });
    }

    getTableRows() {
        const { rows } = this.props;
        const { selected_row_index } = this.state;

        if([] === this.state.action_checkbox_state) {
            return [];
        } else {
            return rows.map((row, index) => {
                let is_popup_active = (this.state.is_action_menu_active && index === selected_row_index);
                return <tr key={v4()}>
                    <td>
                        <PlainRightFloatedCheckbox
                            index={index}
                            checked_state={this.state.action_checkbox_state[index].is_checked}
                            handle_checked_state_change={this.handle_user_action_checkbox_state}
                        />
                    </td>
                    <td>{row.name}</td>
                    <td>{row.roles}</td>
                    <td>{row.department}</td>
                    <td>
                        <SimplePopupBasedMenu
                            direction="right"
                            icon="setting"
                            is_popup_active={is_popup_active}
                            unique_action_identifier={row.key}
                            items={this.pop_menu_items}
                            index={index}
                            handle_open_menu_click={this.open_setting_menu}
                        />
                    </td>
                </tr>
            });
        }
    }

    open_setting_menu(index) {
        if(this.bulk_delete_index === index) {
            this.setState({
                is_bulk_action_popup_open: !this.state.is_bulk_action_popup_open
            });
        } else {
            this.setState({
                is_action_menu_active: !this.state.is_action_menu_active,
                selected_row_index: index
            });
        }
    }

    get_action_table_header() {
        let menu_items = [
            {
                name: utilities.language.bulk_delete,
                action: this.handleDeleteClick
            }
        ];
        if(this.state.action_check_all_checkbox_state) {
            return <SimplePopupBasedMenu
                direction="right"
                icon="setting"
                is_popup_active={this.state.is_bulk_action_popup_open}
                unique_action_identifier={this.bulk_delete_index}
                items={menu_items}
                index={this.bulk_delete_index}
                handle_open_menu_click={this.open_setting_menu}
                label={utilities.language.bulk_action}
            />
        }
        return utilities.language.action
    }

    render() {
        return <table className="ui unstackable celled very basic striped table unstackable_very_basic_striped_table">
            <thead>
                <tr>
                    <th>
                        <PlainRightFloatedCheckbox
                            index={-2}
                            checked_state={this.state.action_check_all_checkbox_state}
                            handle_checked_state_change={this.handle_check_all_check_box_click}
                        />
                    </th>
                    <th>{utilities.language.name}</th>
                    <th>{utilities.language.role}</th>
                    <th>{utilities.language.department}</th>
                    <th>
                        {this.get_action_table_header()}
                    </th>
                </tr>
            </thead>
            <tbody>
                {this.getTableRows()}
            </tbody>
        </table>
    }
}

export default UnstackableVeryBasicStripedUsersTable;