/**
 * Created by anfal on 12/3/2016.
 */

const config = {
    debug: true,
    learnup_server_url: 'http://192.168.8.100:5000',
    auth_server_url: 'http://192.168.8.100:80',
    pagination: {
        max_items_per_page: 15
    }
};

export default config;