import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import Routes from './routes';
import redux_store from './redux';
import './assets/semantic/semantic.min.css';

ReactDOM.render(
  <Provider store={redux_store}>
      <Router history={browserHistory} routes={Routes}/>
  </Provider>,
  document.getElementById('root')
);
