/**
 * Created by anfal on 11/30/2016.
 */

import config from '../../config';

const end_points = {
    auth_url: config.auth_server_url + '/authenticate_user',
    users_url: config.auth_server_url + '/users',
    university_data: config.auth_server_url + '/university_data',
    university_department_list_url: config.learnup_server_url + '/university_config/faculties'
};

export default end_points;