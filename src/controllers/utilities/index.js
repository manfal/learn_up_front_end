/**
 * Created by anfal on 12/30/2016.
 */

import InAppRoutings from './InAppRoutings';
import KeysObject from './KeysObject';
import API from './API';
import EndPoints from './EndPoints';
import Language from './Language';
import MenuItems from './MenuItems';
import SystemUsers from './SystemUsers';

const utilities = {
    in_app_routings: InAppRoutings,
    keys_object: KeysObject,
    api: API,
    end_points: EndPoints,
    language: Language,
    menu_items: MenuItems,
    system_users: SystemUsers
};

export default utilities;