/**
 * Created by anfal on 12/31/2016.
 */

//Make sure system users matches the roles used on server side.
//Make sure system users are in all lower case, even if role sent by system is in upper case
//because auth reducer have already changed it to lower case for consistency.
export default {
    admin: 'admin',
    teacher: 'teacher',
    student: 'student'
}