/**
 * Created by anfal on 12/3/2016.
 */

import { browserHistory } from 'react-router';

class InAppRoutings {

    static getLoginPageRoute() {
        return '/';
    }

    static getDashboardPageRoute() {
        return '/dashboard'
    }

    static redirectUserToDashboard() {
        browserHistory.push(InAppRoutings.getDashboardPageRoute());
    }

    static redirectUserToLoginPage() {
        browserHistory.push(InAppRoutings.getLoginPageRoute());
    }

    static redirectUserToNotFoundPage() {
        browserHistory.push('/page_not_found');
    }

}

export default InAppRoutings;