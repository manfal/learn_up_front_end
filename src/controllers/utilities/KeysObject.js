/**
 * Created by anfal on 12/1/2016.
 */

export default {
    STATUS: 'status',
    MESSAGE: 'message',
    FAIL: 'fail',
    SUCCESS: 'success',
    USER_AUTH_DATA: 'user_auth_data',
    ROLE_ADMIN: 'Admin',
    ADD_USER: 'ADD_USER',
    CREATE_OR_UPDATE_USER: 'CREATE_OR_UPDATE_USER',
    DELETE_USER: 'DELETE_USER',
    EDIT_USER: 'EDIT_USER',
    ENTER_KEY: 13
}