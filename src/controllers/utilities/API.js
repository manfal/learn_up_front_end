/**
 * Created by anfal on 12/30/2016.
 */

import Fetch from 'isomorphic-fetch';

class API {

    static __headers = {
        'Content-Type': 'application/json'
    };

    static GET(url, parameters=null) {
        //Format the parameters string using get method
        //e.g. parameter1=value&parameter2=value
        //because get doesn't know by default on how to format them.
        let final_url = (null === parameters) ? url : `${url}?${parameters}`;
        return Fetch(final_url).then((response) => {
            return response.json();
        });
    }

    static POST(url, data) {
        return Fetch(url, {
            method: 'POST',
            headers: API.__headers,
            body: JSON.stringify(data)
        }).then(response => {
            return response.json();
        });
    }

    static DELETE(url, data) {
        return Fetch(url, {
            method: 'DELETE',
            headers: API.__headers,
            body: JSON.stringify(data)
        }).then(response => {
            return response.json();
        });
    }

}

export default API;