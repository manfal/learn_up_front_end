/**
 * Created by anfal on 12/6/2016.
 */

import SystemUsers from './SystemUsers';
//I could plainly import utilities here, but I wanted to avoid a circular dependency so importing
//Language directly.
import Language from './Language';

let menu_items = {};

menu_items[SystemUsers.admin] = [
    {
        name: Language.home,
        url: '/dashboard'
    },
    {
        name: Language.users,
        url: '/dashboard/users'
    },
    {
        name: Language.departments,
        url: '/dashboard/departments'
    },
    {
        name: Language.course_settings,
        url: '/dashboard/course_settings'
    }
];

export default menu_items;