/**
 * Created by anfal on 11/30/2016.
 */

import utilities from './utilities';

class AuthController {

    static authenticate_user(username, password, remember_me) {
        let parameters = `username=${username}&password=${password}&remember_me=${remember_me}`;
        let url = utilities.end_points.auth_url;
        return utilities.api.GET(url, parameters);
    }

    static store_user_data_in_storage(data_object) {
        let remember_me = data_object.data.remember_me;
        if('true' === remember_me) {
            if(null !== localStorage.getItem(utilities.keys_object.USER_AUTH_DATA)) {
                localStorage.removeItem(utilities.keys_object.USER_AUTH_DATA);
            }
            localStorage.setItem(utilities.keys_object.USER_AUTH_DATA, JSON.stringify(data_object));
        }
    }

    static clear_user_data_from_storage() {
        if(null !== localStorage.getItem(utilities.keys_object.USER_AUTH_DATA)) {
            localStorage.removeItem(utilities.keys_object.USER_AUTH_DATA);
        }
    }

    static get_user_data_object_from_storage() {
        return JSON.parse(localStorage.getItem(utilities.keys_object.USER_AUTH_DATA));
    }

    static verify_user_is_authenticated(is_logged_in_flag) {
        let authentication_object = {
            is_logged_in: false,
            data: null
        };
        if(!is_logged_in_flag) {
            let user_auth_in_storage = AuthController.get_user_data_object_from_storage();
            if(null !== user_auth_in_storage) {
                authentication_object.is_logged_in = true;
                authentication_object.data = user_auth_in_storage;
            }
        } else {
            authentication_object.is_logged_in = is_logged_in_flag;
        }
        return authentication_object;
    }
}

export default AuthController;