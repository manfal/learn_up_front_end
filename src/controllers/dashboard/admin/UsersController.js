/**
 * Created by anfal on 12/12/2016.
 */

import config from '../../../config';
import utilities from '../../utilities';

class UsersController {

    static get_all_users(current_page_index, max_users_per_page, filter_user_text, role) {
        //making it exceed line of sight because it will add foreign characters if space is given.
        let parameters = `current_page=${current_page_index}&max_users_per_page=${max_users_per_page}&filter_user_text=${filter_user_text}&role_filter=${role}`;
        let url = utilities.end_points.users_url;
        return utilities.api.GET(url, parameters);
    }

    static get_single_user_data(user_id) {
        let parameters = `single_user_id=${user_id}`;
        let url = utilities.end_points.users_url;
        return utilities.api.GET(url, parameters);
    }

    static get_university_data() {
        return utilities.api.GET(utilities.end_points.university_data);
    }

    static get_university_department_list() {
        return utilities.api.GET(utilities.end_points.university_department_list_url);
    }

    static delete_users(user_ids) {
        return utilities.api.DELETE(utilities.end_points.users_url, user_ids);
    }

    static parse_users_information_to_array(users_data) {
        let user_info_array = [];
        try {
            for(let user_data of users_data) {
                let user_info = {
                    name: user_data.first_name + " " + user_data.last_name,
                    roles: user_data.roles.join(),
                    department: user_data.department,
                    key: user_data.key
                };
                user_info_array.push(user_info);
            }
        } catch(exception) {

        }
        return user_info_array;
    }

    static insert_new_user(user_data_object) {
        return utilities.api.POST(utilities.end_points.users_url, user_data_object);
    }

    static get_max_number_of_pages_for_pagination(total_number_of_users) {
        let max_pages_for_pagination = 0;
        try {
            max_pages_for_pagination = Math.ceil(total_number_of_users / config.pagination.max_items_per_page);
        } catch(exception) {
        }
        return max_pages_for_pagination;
    }
}

export default UsersController;